import UIKit
import WebKit
import SnapKit

protocol Navigation {
    var parentController: UIViewController? { get set }
    func pop()
    func push(viewControler: UIViewController)
}

struct WebNavigation: Navigation {
    
    var parentController: UIViewController?
    
    func pop() {
        parentController?.navigationController?.popViewController(animated: true)
    }
    
    func push(viewControler: UIViewController) {
        parentController?.navigationController?.pushViewController(viewControler, animated: true)
    }
    
    init() {parentController = nil}
}

struct TestWebViewModel {
    
    enum Action {
        case loadPage(URL)
        case goBack
    }
    
    init() {}
    
    var actionHandler: ((Action) -> Void)?
    
    func handle(url: URL, completion: ((WKNavigationActionPolicy)) -> Void) {
        
    }
    
}

class WebviewViewController: UIViewController, WKNavigationDelegate {
    
    enum Action {
        case load(URL)
        case back
    }
    
    private var webView: WKWebView = WKWebView()
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var viewModel: WebViewModel?
    var navigation: Navigation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(viewModel: WebViewModel, navigation: Navigation) {
        
        self.viewModel = viewModel
        self.navigation = navigation
        super.init(nibName: nil, bundle: nil)
        self.navigation?.parentController = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(webView)
        self.webView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view.safeAreaLayoutGuide.snp.edges)
        }
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.load(URLRequest(url: viewModel!.baseURL))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(viewModel!.checkUrl(url: navigationAction.request.url, navigation: self.navigation!))
    }
    
    @objc func goBack() {
        if webView.canGoBack {
            webView.goBack()
        } else {
            navigation?.pop()
        }
    }
}

extension WebviewViewController.Action {
    init(action: TestWebViewModel.Action) {
        switch action {
        case let .loadPage(url):
            self = .load(url)
        case .goBack:
            self = .back
        }
    }
}

protocol WebViewModel {
    var javascriptCode: String? { get }
    var baseURL: URL { get }
    func checkUrl(url: URL?, navigation: Navigation) -> WKNavigationActionPolicy
}

struct BillPayViewModel: WebViewModel {
    
    
    var javascriptCode: String? {
        return nil
    }
    
    var baseURL: URL {
        return URL(string: "http://apple.com")!
    }
    
    func checkUrl(url: URL?, navigation: Navigation) -> WKNavigationActionPolicy {
        
        if url?.absoluteString.contains("apple.com") == true {
            return .allow
        }
        
        return .cancel
    }
    
}

