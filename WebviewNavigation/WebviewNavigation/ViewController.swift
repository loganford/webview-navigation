import UIKit
import SnapKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        let button = UIButton(type: .custom)
        button.setTitle("Go to Webview", for: .normal)
        button.addTarget(self, action: #selector(openWebview), for: .touchUpInside)
        button.setTitleColor(.blue, for: .normal)
        self.view.addSubview(button)
        button.snp.makeConstraints { (maker) in
            maker.height.equalTo(50)
            maker.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(50)
            maker.center.equalToSuperview()
        }
    }
    
    @objc func openWebview() {
        let vc = WebviewViewController(viewModel: BillPayViewModel(), navigation: WebNavigation())
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

